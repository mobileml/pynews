from setuptools import find_packages, setup


def get_version():
    about = {}
    with open("src/pynews/__version__.py", "r") as f:
        exec(f.read(), about)
    version = about["__version__"]
    return version


setup(
    name="pynews",
    version=get_version(),
    package_dir={"": "src"},
    packages=find_packages("src"),
    python_requires="~=3.7",
    install_requires=["pandas == 1.1.*"],
    extras_require={
        "dev": ["pre-commit", "tox"],
        "dataprep": ["scikit-learn == 0.23.2", "loguru ~= 0.5"],
        "train": ["tensorflow-gpu ~= 2.3", "luigi ~= 3.0", "tensorflowjs ~= 2.3", "pyarrow ~= 1.0"],
    },
    package_data={},
)
