import pytest

from pynews.processing import clean_text, make_sequences, make_vocab

clean_text_examples = [
    # (input, expected output)
    ("follow https://stackoverflow.com/questions/120693/javascript-try-catch me", "follow me"),
    ("e-mail", "mail"),
    ("ALL CAPS and some lower", "all caps and some lower"),
    ("Use 33.4 units", "use num units"),
    ("a! b\" #$%&' (and)* r+t, b-b. /:c <=> ah? top@secret.com \\^ a_a `code` {b|r} ~n", "and ah top secret com code"),
    ("he ☺", "he"),
    ("unit123456 unit89", "unit1 unit1"),
]

vocab_sentences = [
    "i am testing code with pytest but they do not seem to make use of ctypes",
    "i am not yet sure if any real use would be possible at this time",
]


@pytest.mark.parametrize("text,expected", clean_text_examples)
def test_clean_text(text, expected):
    assert clean_text(text) == expected


def test_make_vocab():
    sentences = [x.split(" ") for x in vocab_sentences]
    vocab = make_vocab(sentences)
    assert len(vocab) == 29
    assert "<PAD>" in vocab
    assert "testing" in vocab
    assert "code" in vocab


def test_make_sequences():
    sentences = [x.split(" ") for x in vocab_sentences]
    vocab = make_vocab(sentences)
    sequences = make_sequences(sentences, vocab)

    assert len(sequences) == len(sentences)
    assert len(sequences[0]) == len(sentences[0])
