"""This module is used to store sample of news20 dataset into a sqlite database"""
import sqlite3

import numpy as np
import pandas as pd
from loguru import logger
from sklearn.datasets import fetch_20newsgroups


def main():
    # Settings:

    # 1. we will use just a few categories
    categories = [
        "comp.os.ms-windows.misc",
        "comp.sys.ibm.pc.hardware",
        "comp.sys.mac.hardware",
        "comp.windows.x",
        "rec.autos",
        "rec.motorcycles",
        "sci.med",
        "sci.space",
    ]
    # 2. parts of messages we will remove, i.e. text pre-processing
    remove = ("headers", "quotes")

    # 3. database filename
    db_file = "news20.sqlite"

    # Code below:
    logger.info(f"Loading newsgroup20 dataset for categories:\n{categories}")

    # Fetch training data and convert it to pandas DF
    newsgroups_train = fetch_20newsgroups(subset="train", categories=categories, remove=remove)
    train_df = pd.DataFrame({"Data": newsgroups_train.data, "Category": newsgroups_train.target})
    train_df = train_df.assign(Year=np.random.randint(low=1922, high=1953, size=(len(newsgroups_train.target), 1)))

    # Fetch test data and convert it to pandas DF
    newsgroups_test = fetch_20newsgroups(subset="test", categories=categories, remove=remove)
    test_df = pd.DataFrame({"Data": newsgroups_test.data, "Category": newsgroups_test.target})
    test_df = test_df.assign(Year=np.random.randint(low=1990, high=2000, size=(len(newsgroups_test.target), 1)))

    # Make one DF of all data
    df = pd.concat([train_df, test_df])

    # Convert targets from int to string
    class_names = {idx: value for idx, value in enumerate(newsgroups_train.target_names)}
    df.Category = df.Category.map(class_names)

    # Convert pandas DF to sqlite database
    conn = sqlite3.connect(db_file)

    logger.info(f"Saving news20 into the database: {db_file}")
    df.to_sql("NEWS", conn, if_exists="replace", index=False)
    logger.info("Done")


if __name__ == "__main__":
    main()
