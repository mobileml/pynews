import datetime
import json
import logging
from pathlib import Path

import joblib
import luigi
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflowjs as tfjs
from tensorflow.python.keras.callbacks import ModelCheckpoint

from ..ml.model import make_model
from ..processing import MAX_SEQUENCE_LENGTH, PAD_ID, make_sequences, make_vocab
from .functions import fetch_data, prepare_data, score_recall_at_n

logger = logging.getLogger(__name__)
np.random.seed(112)


def get_build_dir(date: datetime.date) -> Path:
    """Get directory for storing artifacts

    This directory is controlled by configuration and padded with the current date
    """
    config = luigi.configuration.get_config()
    return Path(config["core"]["builds"]) / str(date)


def get_db():
    import sqlite3

    config = luigi.configuration.get_config()
    return sqlite3.connect(config["database"]["database"])


class FetchData(luigi.Task):
    """Fetch raw data from the database"""

    date = luigi.DateParameter(default=datetime.date.today())
    # This just mimics a basic filtering on the DB side
    cutoff_year = luigi.IntParameter(default=1925)
    path: str = "sources/raw_data.parquet"

    def output(self):
        path = get_build_dir(self.date) / self.path
        return luigi.LocalTarget(str(path))

    def run(self):
        logger.info("Fetching features from the database")
        df = fetch_data(get_db(), self.cutoff_year)
        with self.output().temporary_path() as fn:
            df.to_parquet(fn)

    def load(self):
        return pd.read_parquet(self.output().path)


class MakeFeatures(luigi.Task):
    """Process raw data and generate training and testing sets"""

    date = luigi.DateParameter(default=datetime.date.today())
    # Starting year from which we will consider data for our test set
    tests_year = luigi.IntParameter(default=1990)

    def requires(self):
        return FetchData(date=self.date)

    def load(self):
        loaded = {}
        for key, value in self.output().items():
            if "parquet" in value.path:
                loaded[key] = pd.read_parquet(value.path)
            else:
                loaded[key] = joblib.load(value.path)
        return loaded

    def output(self):
        base = get_build_dir(self.date)
        outputs = {
            "vocab": "vocabulary.pickle",
            "train": "sources/train.pickle",
            "test": "sources/test.pickle",
            "features": "sources/features.parquet",
        }
        for key, value in outputs.items():
            outputs[key] = luigi.LocalTarget(str(base / value))
        return outputs

    def run(self):
        from tensorflow.keras.preprocessing.sequence import pad_sequences

        raw_features = self.requires().load()
        features = prepare_data(raw_features, self.tests_year)

        train_data = features.query("TestSet == False")
        test_data = features[features.TestSet]

        words_vocab = make_vocab(train_data.Words)

        # we do not need unique and padding in tags vocabulary, so we make the vocabulary right here
        tags_vocab = {cat: idx for idx, cat in enumerate(train_data.Category.unique())}

        train_seq = make_sequences(train_data.Words, words_vocab)
        test_seq = make_sequences(test_data.Words, words_vocab)

        train_X = pad_sequences(train_seq, maxlen=MAX_SEQUENCE_LENGTH, value=PAD_ID, padding="post", truncating="post",)
        test_X = pad_sequences(test_seq, maxlen=MAX_SEQUENCE_LENGTH, value=PAD_ID, padding="post", truncating="post",)

        # categories between train and test should be the same
        assert np.array_equal(set(train_data.Category.unique()), set(test_data.Category.unique()))
        train_y = train_data.Category.map(tags_vocab)
        test_y = test_data.Category.map(tags_vocab)

        with self.output()["vocab"].temporary_path() as fn:
            joblib.dump((words_vocab, tags_vocab), fn)
        with self.output()["train"].temporary_path() as fn:
            joblib.dump((train_X, train_y), fn)
        with self.output()["test"].temporary_path() as fn:
            joblib.dump((test_X, test_y), fn)
        with self.output()["features"].temporary_path() as fn:
            features.to_parquet(fn)


class TrainModel(luigi.Task):
    date = luigi.DateParameter(default=datetime.date.today())
    epochs = luigi.IntParameter(default=50)
    batch_size = luigi.IntParameter(default=64)
    embedding_dim = luigi.IntParameter(default=32)

    @property
    def path(self):
        return "model"

    @property
    def best_model_path(self) -> str:
        return str(get_build_dir(self.date) / "best_model.h5")

    def load(self):
        return tf.keras.models.load_model(self.output().path)

    def requires(self):
        return MakeFeatures(date=self.date)

    def output(self):
        path = get_build_dir(self.date) / self.path
        return luigi.LocalTarget(str(path), luigi.format.Nop)

    def run(self):
        # Load the data
        train_X, train_y = joblib.load(self.input()["train"].path)
        test_X, test_y = joblib.load(self.input()["test"].path)
        words_vocab, tags_vocab = joblib.load(self.input()["vocab"].path)

        assert len(train_X) == len(train_y)
        logger.debug(f"# of rows in train: {len(train_X)}")

        # Define callbacks
        earlystop_callback = tf.keras.callbacks.EarlyStopping(monitor="val_accuracy", min_delta=0.001, patience=5)
        mc = ModelCheckpoint(
            self.best_model_path, monitor="val_accuracy", mode="max", verbose=1, save_weights_only=True, save_best_only=True,
        )

        # Prepare the model
        model = make_model(len(words_vocab), len(tags_vocab), embedding_dim=self.embedding_dim, input_length=MAX_SEQUENCE_LENGTH)
        model.compile(
            loss="sparse_categorical_crossentropy", optimizer="adam", metrics=["accuracy"],
        )

        logger.info("Training classifier...")
        model.fit(
            train_X,
            train_y,
            epochs=self.epochs,
            batch_size=self.batch_size,
            validation_data=(test_X, test_y),
            callbacks=[mc, earlystop_callback],
        )

        # Load the best model in order to resave it according to our folder structure
        model.load_weights(self.best_model_path)
        try:
            Path(self.best_model_path).unlink()
        except Exception as e:
            logger.warning(f"Failed to delete {self.best_model_path}. Error: {repr(e)}")

        with self.output().temporary_path() as fn:
            model.save(fn)


class EvaluateModel(luigi.Task):
    """Evaluate a trained model using test data"""

    date = luigi.DateParameter(default=datetime.date.today())

    def requires(self):
        return (
            TrainModel(date=self.date),
            MakeFeatures(date=self.date),
        )

    def output(self):
        path_score_report = get_build_dir(self.date) / Path("pynews_mlscores.mlscore")
        return luigi.LocalTarget(str(path_score_report))

    def run(self):
        inputs = self.input()
        inp_features = inputs[1]

        test_X, test_y = joblib.load(inp_features["test"].path)
        logger.info(f"Shape of pynews dataset loaded for evaluation: {test_X.shape}")

        model = tf.keras.models.load_model(inputs[0].path)

        n_list = [1, 3, 5]
        recall_at_1_3_5 = score_recall_at_n(n_list, model, test_X, test_y)
        mlscore_result_list = []
        for n, recall_at_n in zip(n_list, recall_at_1_3_5):
            mlscore_report = {
                "DataSet": "News20",
                "Time": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
                "Label": f"Recall@{n}",
                "Value": recall_at_n,
            }
            logger.debug(str(mlscore_report))
            mlscore_result_list.append(mlscore_report)
            logger.info(f"Recall@{n} metric is {100*recall_at_n:.2f} %.")

        with self.output().temporary_path() as fn:
            with open(fn, "w") as fo:
                json.dump(mlscore_result_list, fo, indent=2)


class ConvertToJs(luigi.Task):
    """Convert trained model to tensorflow js"""

    date = luigi.DateParameter(default=datetime.date.today())
    js_dir = luigi.Parameter(
        default="", description="Folder to put converted JS model. If omitted, then build directory is used."
    )
    num_examples = luigi.IntParameter(default=100, description="Number of example texts exported for the web application")

    def requires(self):
        return {
            "model": TrainModel(date=self.date),
            "features": MakeFeatures(date=self.date),
            "raw_data": FetchData(date=self.date),
        }

    def output(self):
        base_folder = get_build_dir(self.date)
        if self.js_dir:
            base_folder = Path(self.js_dir)
        return {
            "model_dir": luigi.LocalTarget(str(base_folder)),
            "constants": luigi.LocalTarget(str(base_folder / "constants.js")),
        }

    def run(self):
        inputs = {key: task.load() for key, task in self.requires().items()}
        input_features = inputs["features"]
        words_vocab, tags_vocab = input_features["vocab"]
        model = inputs["model"]
        # Take `num_examples` random samples from test set as example data
        features: pd.DataFrame = inputs["raw_data"]
        idx = np.random.randint(0, high=features.shape[0], size=(self.num_examples,))
        example_data = features.iloc[idx].reset_index().Data.to_json(indent=2)

        # this method saves model as layered model
        tfjs.converters.save_keras_model(model, self.output()["model_dir"].path)

        with self.output()["constants"].temporary_path() as fn:
            with open(fn, "w") as fo:
                fo.write("const WORDS_VOCAB = {\n")
                for line in json.dumps(words_vocab)[1:-1].split(","):
                    fo.write(f"\t{line},\n")
                fo.write("};\n")

                fo.write("const TAGS_VOCAB = {\n")
                for line in json.dumps(tags_vocab)[1:-1].split(","):
                    fo.write(f"\t{line},\n")
                fo.write("};\n")

                fo.write(f"const EXAMPLE_DATA = {example_data};\n")

                fo.write(f"const MAX_SEQUENCE_LENGTH = {MAX_SEQUENCE_LENGTH};\n")


class Train(luigi.WrapperTask):
    """Execute a complete model training workflow"""

    date = luigi.DateParameter(default=datetime.date.today())

    def requires(self):
        yield TrainModel(date=self.date)
        yield EvaluateModel(date=self.date)
        yield ConvertToJs(date=self.date)
