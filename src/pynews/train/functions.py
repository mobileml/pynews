import logging
from typing import Iterable, Optional, Sequence

import numpy as np
import pandas as pd

from ..processing import clean_text

logger = logging.getLogger(__name__)


def fetch_data(conn, cutoff_year: Optional[int] = None) -> pd.DataFrame:
    sql = """SELECT Data, Category, Year
        FROM NEWS
        WHERE year >= ?
        """
    if not cutoff_year:
        cutoff_year = 0
    return pd.read_sql(sql, conn, params=[cutoff_year])


def prepare_data(df: pd.DataFrame, tests_year: int) -> pd.DataFrame:
    logger.debug("Starting data preprocessing for 'pynews'")
    logger.debug(f"Original shape of DataFrame: {df.shape}")

    # drop news without data
    df.Data = df.Data.replace("", np.nan)
    df = df.dropna(subset=["Data", "Category"], how="any")

    # let's split to train and test sets based on `tests_year`
    df = df.assign(TestSet=df.Year >= tests_year)

    df.Data = df.Data.apply(clean_text)
    df = df.assign(Words=df.Data.apply(lambda x: x.split(" ")))
    return df


def score_recall_at_n(n_list: Sequence[int], model, x_test: np.ndarray, y_test: Iterable[int]):
    """"Calculate recall score in a micro averaging manner"""
    probs = model.predict(x_test)
    num_classes = model.outputs[0].shape[1]
    classes = np.array(range(num_classes))
    y_preds = {}
    recall_at_n_bool = {}
    for n in n_list:
        y_preds[str(n)] = []
        recall_at_n_bool[str(n)] = []
    for prob, y_true in zip(probs, y_test):
        for n in n_list:
            best_n = np.argsort(prob, axis=0)[-n:][::-1]
            key = str(n)
            recall_at_n_bool[key].append(y_true in best_n)
            try:
                y_preds[key].append(list(classes[best_n]))
            except Exception as e:
                logger.warning(repr(e))
                logger.debug(f"n: {n}, processed: {len(y_preds[key])}")
                raise e

    return [np.mean(recall_at_n_bool[n]) for n in recall_at_n_bool.keys()]
