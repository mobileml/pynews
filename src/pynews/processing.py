import re

# max number of words per sample. In order to figure out what this number might be,
# have a look at distribution of number of words per sample in your dataset.
from typing import Mapping, Sequence

MAX_SEQUENCE_LENGTH = 150
PAD_ID = 0
UNK_ID = 1


def clean_text(text: str) -> str:
    text = text.lower().strip()
    # Remove hyperlinks
    text = re.sub(r"(www|http[s]?://)(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", "", text,)

    # Replace all punctuations with ' ':
    text = re.sub("[!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]", " ", text)

    # Replace all pure numbers (int and float) with num
    text = re.sub(r"\b\d+[.,]*\d+\b", "num", text)

    # Remove all non-standard symbols/characters
    text = re.sub(r"[^A-Za-z0-9,.?]", " ", text)
    # Remove all single letters
    text = re.sub(r"\b[\w]\b", "", text)
    # Replace two or more spaces by a single space
    text = re.sub(r"\s\s+", " ", text)

    # Replace all digit sequences by a single digit
    text = re.sub(r"\d+", "1", text)

    # Concatinatiot n
    text = " ".join(text.split())
    text = text.encode("iso-8859-1", "ignore").decode("iso-8859-1", "ignore")

    return text.strip()


def make_vocab(lists_of_words: Sequence[Sequence[str]]) -> Mapping[str, int]:
    """Generate vocabulary from texts

    Generated vocabulary is very simple. Each word gets its own numeric ID.

    Args:
        lists_of_words: Double list of words. The list contains of sentences where each sentence is a list of words.
    Returns:
        Vocabulary dictionary that can be used to convert words to numbers.
    """
    vocab = {"<UNK>": UNK_ID, "<PAD>": PAD_ID}
    idd = max([PAD_ID, UNK_ID]) + 1
    for sen in lists_of_words:
        for word in sen:
            if word not in vocab:
                vocab[word] = idd
                idd += 1

    return vocab


def make_sequences(lists_of_words: Sequence[Sequence[str]], vocab: Mapping[str, int]) -> Sequence[Sequence[int]]:
    """Convert words (text) into numeric values based on vocabulary

    Args:
        lists_of_words: Double list of words. First dimension is the number of sentences. Second dimension - number of
            words in a sentence.
        vocab: Vocabulary dictionary, which is used to map strings to numbers.

    Returns:
        list of sentences where each sentence consists of list of numbers.
    """
    sequences = []
    for words in lists_of_words:
        seq = []
        for word in words:
            seq.append(vocab.get(word, UNK_ID))
        sequences.append(seq)
    return sequences
