"""pynews model definition.

Model to predict a category from a text.

"""
import tensorflow as tf


def make_model(words_vocab_size: int, tags_vocab_size: int, input_length: int, embedding_dim: int = 64):
    model = tf.keras.Sequential(
        [
            tf.keras.layers.Embedding(words_vocab_size + 1, embedding_dim, input_length=input_length),
            tf.keras.layers.GlobalAveragePooling1D(),
            tf.keras.layers.Dense(embedding_dim * 2, activation="relu"),
            tf.keras.layers.Dense(tags_vocab_size, activation="softmax"),
        ]
    )
    print(model.summary())
    return model
