FROM python:3.8-buster

ARG PIP_NO_CACHE_DIR=on
WORKDIR /pynews

# Expose environment variable that points to website assets folder
ENV PYNEWS_JS_DIR=/pynews/client-tf/static/assets

# Install nodejs 14 and client web application
RUN apt-get update && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update && \
    ACCEPT_EULA=Y apt-get install -y nodejs && \
    apt-get clean && \
    python -m pip install --upgrade pip setuptools && \
    git clone https://gitlab.com/mobileml/client-tf.git

# Run npm install during docker build
# Depending on your docker setup, it may fail if you do it during container run
RUN cd client-tf && \
    npm install && \
    npm cache clean --force && \
    cd /pynews

COPY . ./

# Install Python package from sources
RUN pip install --use-feature=2020-resolver .[dataprep,train] && \
    # create database
    python -m pynews.news_to_sqlite

EXPOSE 80
CMD set -e && \
    luigi --local-scheduler --module pynews.train.tasks Train && \
    cd client-tf && \
    node server.js
