# Text classification pipeline in Python (Python vs Node.js series)

[Python vs Node.js series](https://gitlab.com/mobileml) demonstrates the differences between training a machine learning model in different languages. The end goal
is to serve the trained model in a browser. Python code (this repo) is used as a reference baseline.

The project is to perform text classification on a subset (eight categories) of news20 dataset. The code doesn't focus on
the model itself, but rather on the pipeline around it. The motivation behind this approach is to go one step further from the
basic how-to examples where the data is nicely prepared in two files. *Note that there are still some things one would normally do before taking such code to production.*

This project reads data from a database (sqlite is used for simplicity) with an additional filtering on the database side.
The data is processed with help of Luigi pipeline framework. The model is evaluated with metric recall@n, which again simulates
a real use scenario.

Database contains a single table NEWS:
```
CREATE TABLE IF NOT EXISTS "NEWS" (
  "Data" TEXT,
  "Category" TEXT,
  "Year" INTEGER
);
```
where `Data` is post's data, `Category` is post's topic and `Year` is an artificial random year. We use `year` to separate training data from the test data.

## Docker

The docker image trains the model and runs a simple website that demonstrates model usage.

```
# Build image
docker build -t tf_demo .
# Run it. After training is done, visit http://localhost:5000/
docker run --rm -itp 5000:80 tf_demo

# If you want to preserve built model
docker run --rm -itv <full_path_to_local_dir>:/pynews/builds -p 5000:80 tf_demo
```

Read further for local machine development.

## Installation

Prerequisites:

- Python 3.6+ with pip 19+

### Database creation

We have to create the databse with our dataset that we will use afterwards. This is a separate step, not related to the main code of model creation.

Install `pynews` package in editable mode:

```
pip install -e .[dataprep,train]
python -m pynews.news_to_sqlite
```

This will create file `news20.sqlite` in the current directory. After you have the database, you may proceed with the further steps.

## Development

```
pip install -e .[dev,dataprep,train]
pre-commit install
```

### Testing

```bash
# NOTE: Package must be installed with the [dev] extra
tox
```

## Usage

### Training

It is recommended to run all the commands from the package root path. Otherwise you will need to specify luigi configuration path:

```
export LUIGI_CONFIG_PATH=path/to/my_custom_config.cfg  # Bash or similar
set LUIGI_CONFIG_PATH=path/to/my_custom_config.cfg     # Windows CMD
$Env:LUIGI_CONFIG_PATH='./config/my_custom_config.cfg' # PowerShell
```

To start the training process, including any unmet dependency in fetch and/or process:
```
luigi --local-scheduler --module pynews.train.tasks Train
```

The `luigi.cfg` file may be used to customize the training process.

### Prediction

The project focuses on prediction from a browser. There is no convenient code to
do prediction from the python code. See docker file and [client-tf](https://gitlab.com/mobileml/client-tf) for the insights about how to predict data.

## Versioning

The `pynews` Python package has a version number set in `src/pynews/__version__.py`.

Given a version number MAJOR.MINOR.MICRO, increment the:

* MAJOR if you introduce backward incompatible changes in `pynews.model` module,
* MINOR if you introduce changes which do not affect the `pynews.model` module,
e.g. if you improve feature extraction logics.
* MICRO if you introduce bug fixes.

Create a git tag equal to `pynews` version for each commit that is used in production.
